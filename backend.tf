# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}

#Use S3 as backend
terraform {
  backend "s3" {
    bucket = "mr-terraform-state"
    key    = "infrastructure"
    region = "us-east-1"
    dynamodb_table = "terraform-state-lock-dynamo"
  }
}
