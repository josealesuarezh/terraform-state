terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}


# S3 bucket for storing tf state
resource "aws_s3_bucket" "tf_bucket" {
  bucket = "mr-terraform-state"
  acl    = "private"

  versioning {
    enabled = true
  }
}

#Block public acces to s3 bucket
resource "aws_s3_bucket_public_access_block" "tf_bucket" {
  bucket                    = aws_s3_bucket.tf_bucket.id
  block_public_acls         = true
  block_public_policy       = true
  ignore_public_acls        = true
  restrict_public_buckets   = true
}

#Create a dynamodb table for locking the state file
resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
  name = "terraform-state-lock-dynamo"
  hash_key = "LockID"
  read_capacity = 1
  write_capacity = 1

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name = "DynamoDB Terraform State Lock Table"
  }
}